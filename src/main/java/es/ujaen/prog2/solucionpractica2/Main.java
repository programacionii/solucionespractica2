/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.solucionpractica2;

import java.io.IOException;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {

        CentroVacunacion centro = new CentroVacunacion("Jaen Norte - Linares", "Avenida Europa 1", "Linares237", "953456789", "Linares237@sspa.es");
        
        Empleado admin = new Empleado("ADMIN", "Administrador", "1234", "612345678", "20/11/2017", "12345678A", 32);
        Empleado emp1 = new Empleado("alluque", "Adrian Luque", "4321", "623456789", "29/11/2017", "23456781B", 30);
        Empleado emp2 = new Empleado("pepito", "Pepito Perez", "2143", "634567890", "05/12/2017", "34567812C", 25);
        centro.añadirEmpleado(admin);
        centro.añadirEmpleado(emp1);
        centro.añadirEmpleado(emp2);

        LoteDosis lote1 = new LoteDosis(23, "Pfizer", 10);
        LoteDosis lote2 = new LoteDosis(47, "Moderna", 13);
        LoteDosis lote3 = new LoteDosis(51, "Pfizer", 6);
        LoteDosis lote4 = new LoteDosis(103, "AstraZeneca", 21);
        centro.añadirLoteVacunas(lote1);
        centro.añadirLoteVacunas(lote2);
        centro.añadirLoteVacunas(lote3);
        centro.añadirLoteVacunas(lote4);

        Usuario usu1 = new Usuario("2310123456", "Alberto García Gonzalez", "98765432A", "Calle Falsa 123", "698765432", 18);
        Usuario usu2 = new Usuario("2310234567", "Carmen Lopez Sanchez", "87654321A", "Calle Nula 456", "687654321", 23);
        Usuario usu3 = new Usuario("2310345678", "Maria Gomez Jimenez", "76543212A", "Calle Invisible 789", "676543212", 27);
        Usuario usu4 = new Usuario("2310456789", "Jose Ruiz Moreno", "65432123A", "Calle Inexistente 147", "665432123", 22);
        centro.añadirUsuario(usu1);
        centro.añadirUsuario(usu2);
        centro.añadirUsuario(usu3);
        centro.añadirUsuario(usu4);
        
        boolean resultado1 = centro.vacunarUsuario(0, 3, 1);
        boolean resultado2 = centro.vacunarUsuario(1, 2, 2);

        if (!resultado1) {
            System.out.println("Vacunación incorrecta del usuario 1");
        }
        if (!resultado2) {
            System.out.println("Vacunación incorrecta del usuario 2");
        }
        
      
        Menu menu = new Menu(centro);
        boolean logueado;

        do {

            logueado = menu.menuLogin();
            if (!logueado) {
                System.out.println("Usuario y/o contraseña incorrectos.");
            } else {
                System.out.println("Login correcto.");
            }
        } while (!logueado);

        menu.menuPrincipal();

    }

}
