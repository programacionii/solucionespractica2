/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.solucionpractica2;

import java.util.ArrayList;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class CentroVacunacion {

    private String nombre;
    private String direccion;
    private String identificador;
    private String telefono;
    private String correo;
    private ArrayList<Empleado> empleados;
    private ArrayList<LoteDosis> lotes;
    private ArrayList<Usuario> usuarios;

    public CentroVacunacion(String nombre, String direccion, String identificador, String telefono, String correo) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.identificador = identificador;
        this.telefono = telefono;
        this.correo = correo;
        this.empleados = new ArrayList<>();
        this.lotes = new ArrayList<>();
        this.usuarios = new ArrayList<>();
    }

    public Empleado login(String user, String pass) {

        for (int i = 0; i < empleados.size(); i++) {
            if (empleados.get(i).getIdentificador().equals(user)) {
                if (empleados.get(i).comprobarContraseña(pass)) {
                    return empleados.get(i);
                }
            }
        }

        return null;

    }

    public boolean añadirLoteVacunas(LoteDosis lote) {
        lotes.add(lote);
        return true;
    }

    public boolean añadirEmpleado(Empleado emp) {
        empleados.add(emp);
        return true;
    }

    public boolean añadirUsuario(Usuario usu) {
        usuarios.add(usu);
        return true;
    }

    public boolean vacunarUsuario(int indiceUsu, int indiceLote, int nDosis) {

        if (indiceUsu < 0 || indiceUsu >= usuarios.size()) { //Usuario no válido
            return false;
        }
        if (usuarios.get(indiceUsu).yaVacunado()) {//Usuario ya vacunado
            return false;
        }
        if (indiceLote < 0 || indiceLote >= lotes.size()) {//Lote no válido
            return false;
        }
        if (lotes.get(indiceLote).getDosisDisponibles() < nDosis) {//Dosis insuficientes
            return false;
        }

        usuarios.get(indiceUsu).registrarVacuna(lotes.get(indiceLote), nDosis);

        lotes.get(indiceLote).retirarDosis(nDosis);
        
        if (lotes.get(indiceLote).getDosisDisponibles() == 0) {
            lotes.remove(indiceLote);
        }

        return true;

    }

    public void mostrarInfoLoteVacunas() {

        System.out.println("Lotes de vacunas disponibles");

        for (int i = 0; i < lotes.size(); i++) {
            System.out.println(lotes.get(i).toString());
        }

        System.out.println();

    }

    public void mostrarInfoLoteVacunasConNumero() {

        System.out.println("Lotes de vacunas disponibles:");

        for (int i = 0; i < lotes.size(); i++) {
            System.out.println("Opción " + i + " " + lotes.get(i).toString());
        }

        System.out.println();

    }

    public void mostrarInfoUsuarios() {

        System.out.println("Usuarios registrados");

        for (int i = 0; i < usuarios.size(); i++) {
            System.out.println(usuarios.get(i).toString());
        }

        System.out.println();

    }

    public void mostrarInfoUsuariosConNumero() {

        System.out.println("Usuarios registrados");

        for (int i = 0; i < usuarios.size(); i++) {
            System.out.println("Opción " + i + " " + usuarios.get(i).toString());
        }

        System.out.println();

    }

    public void mostrarInfoEmpleados(boolean admin) {

        System.out.println("Empleados del centro");

        for (int i = 0; i < empleados.size(); i++) {
            System.out.print(empleados.get(i).toString());
            if (admin) {
                System.out.print(" fechaInc: " + empleados.get(i).getFechaIncorporacion() + " dni: " + empleados.get(i).getDni() + " edad: " + empleados.get(i).getEdad());
            }
            System.out.println();
        }

        System.out.println();

    }

    public void mostrarInfoEmpleadosConNumero(boolean admin) {

        for (int i = 0; i < empleados.size(); i++) {
            System.out.println("Opción: " + i + " " + empleados.get(i).toString());
        }

        System.out.println();

    }

    public boolean eliminarLoteDosisPorId(int idLote) {

        boolean encontradoYEliminado = false;

        for (int i = 0; i < lotes.size(); i++) {
            if (lotes.get(i).getIdentificador() == idLote) {
                lotes.remove(i);
                encontradoYEliminado = true;
            }
        }

        return encontradoYEliminado;
    }

    public boolean eliminarLoteDosisPorIndice(int indice) {

        if (indice < 0) {
            return false;
        }

        if (indice >= lotes.size()) {
            return false;
        }

        lotes.remove(indice);
        return true;

    }

    public boolean darEmpleadoDeBaja(int indiceEmpleado) {

        if (indiceEmpleado >= empleados.size()) {
            return false;
        }

        if (indiceEmpleado == 0) {
            return false;
        }

        empleados.remove(indiceEmpleado);
        return true;
    }

    public boolean darEmpleadoDeBaja(String identificadorUsuario) {

        if (identificadorUsuario.equals("ADMIN")) {
            return false;
        }

        boolean encontradoYEliminado = false;

        for (int i = 0; i < empleados.size(); i++) {
            if (empleados.get(i).getIdentificador().equals(identificadorUsuario)) {

                empleados.remove(i);
                encontradoYEliminado = true;
            }
        }

        return encontradoYEliminado;
    }

    public int comprobarUsuario(int indiceUsu) {

        if (indiceUsu < 0 || indiceUsu >= usuarios.size()) { //Usuario no válido
            return 0;
        }
        if (usuarios.get(indiceUsu).yaVacunado()) {//Usuario ya vacunado
            return 2;
        }
        return 1;//Usuario correcto

    }

    public int comprobarLote(int indiceLote) {

        if (indiceLote < 0 || indiceLote >= lotes.size()) {//Lote no válido
            return 0;
        }
        return 1;//Lote correcto

    }
    
    

    public int comprobarDosisEnElLote(int indiceLote, int nDosis) {

        if (nDosis <= 0 || lotes.get(indiceLote).getDosisDisponibles() < nDosis) {//Dosis insuficientes
            return 0;
        }
        return 1;//Lote correcto

    }

}
