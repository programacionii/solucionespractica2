/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.solucionpractica2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Menu {

    CentroVacunacion centro;
    Empleado usuarioLogueado;

    public Menu(CentroVacunacion centro) {
        this.centro = centro;
        usuarioLogueado = null;
    }

    public boolean menuLogin() throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Bienvenido al sistema, introduzca su usuario:");
        String user = br.readLine();

        while (user.equals("")) {
            System.out.println("Error en el usuario, introduzca de nuevo el usuario:");
            user = br.readLine();
        }

        System.out.println("Introduzca su contraseña:");
        String pass = br.readLine();

        while (pass.equals("")) {
            System.out.println("Error en la contraseña, introduzca de nuevo la contraseña:");
            pass = br.readLine();
        }

        usuarioLogueado = centro.login(user, pass);

        if (usuarioLogueado == null) {
            return false;
        } else {
            return true;
        }

    }

    public void menuPrincipal() throws IOException {

        int seleccion = -1;

        do {

            System.out.println("");
            System.out.println("Seleccione una opción:");
            System.out.println("1- Mostrar info empleados.");
            System.out.println("2- Mostrar info lotes de dosis.");
            System.out.println("3- Mostrar info usuarios.");
            System.out.println("4- Añadir lote dosis.");
            System.out.println("5- Dar de alta usuario.");
            System.out.println("6- Vacunar usuario.");
            
            if (usuarioLogueado.isAdmin()) {
                System.out.println("10- Opción para admin que no hace nada.");
                System.out.println("11- Dar de baja a empleados manual.");
                System.out.println("12- Dar de baja a empleados seleccion.");
                System.out.println("13- Eliminar lotes de dosis manual.");
                System.out.println("14- Eliminar lotes de dosis seleccion.");
                System.out.println("15- Dar de alta empleado.");

            }
            System.out.println("0- Salir.");

            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

            try {
                String lectura = br.readLine();
                seleccion = Integer.parseInt(lectura);
            } catch (NumberFormatException ex) {
                //Si salta la excepción es porque la lectura no es un numero entero
                seleccion = -1;
            }

            switch (seleccion) {

                case 0:
                    System.out.println("Adios.");
                    break;

                case 1:
                    System.out.println("");
                    System.out.println("Menu -> Inforamción Empleados");
                    centro.mostrarInfoEmpleados(usuarioLogueado.isAdmin());
                    break;

                case 2:
                    System.out.println("");
                    System.out.println("Menu -> Información Vacunas");
                    centro.mostrarInfoLoteVacunas();
                    break;

                case 3:
                    System.out.println("");
                    System.out.println("Menu -> Información Usuarios");
                    centro.mostrarInfoUsuarios();
                    break;

                case 4:
                    menuAñadirVacunas();
                    break;

                case 5:
                    menuAñadirUsuario();
                    break;

                case 6:
                    vacunarUsuario();
                    break;

                case 10:
                    if (usuarioLogueado.isAdmin()) {
                        menuAdmin();
                    } else {
                        System.out.println("Opción incorrecta.");
                    }
                    break;

                case 11:
                    if (usuarioLogueado.isAdmin()) {
                        bajaEmpleadoManual();
                    } else {
                        System.out.println("Opción incorrecta.");
                    }
                    break;

                case 12:
                    if (usuarioLogueado.isAdmin()) {
                        bajaEmpleadoSeleccion();
                    } else {
                        System.out.println("Opción incorrecta.");
                    }
                    break;

                case 13:
                    if (usuarioLogueado.isAdmin()) {
                        eliminarLoteDosisManual();
                    } else {
                        System.out.println("Opción incorrecta.");
                    }
                    break;

                case 14:
                    if (usuarioLogueado.isAdmin()) {
                        eliminarLoteDosisSeleccion();
                    } else {
                        System.out.println("Opción incorrecta.");
                    }
                    break;

                case 15:
                    if (usuarioLogueado.isAdmin()) {
                        menuAñadirEmpleado();
                    } else {
                        System.out.println("Opción incorrecta.");
                    }
                    break;

                default:
                    System.out.println("Opción incorrecta.");
                    break;

            }

        } while (seleccion != 0);

    }

    public void eliminarLoteDosisManual() {

        Scanner sc = new Scanner(System.in);

        System.out.println("");
        System.out.println("Menu -> Eliminar Lote");
        System.out.println("Introduce el id de lote a eliminar: ");
        try {
            int idLote = sc.nextInt();
            sc.nextLine();

            while (idLote < 0) {
                System.out.println("Error. Introduce el id de lote a eliminar: ");
                idLote = sc.nextInt();
                sc.nextLine();
            }

            boolean correcto = centro.eliminarLoteDosisPorId(idLote);

            if (correcto) {
                System.out.println("Orden ejecutada correctamente.");
            } else {
                System.out.println("Lote no existente");
            }
        } catch (Exception e) {

            System.out.println("Formato incorrecto.");

        }
    }

    public void eliminarLoteDosisSeleccion() {

        centro.mostrarInfoLoteVacunasConNumero();
        Scanner sc = new Scanner(System.in);

        System.out.println("");
        System.out.println("Menu -> Eliminar Lote");
        System.out.println("Introduce la opción a eliminar: ");
        try {
            int indice = sc.nextInt();
            sc.nextLine();

            while (indice < 0) {
                System.out.println("Error. Introduce la opción a eliminar: ");
                indice = sc.nextInt();
                sc.nextLine();
            }

            boolean correcto = centro.eliminarLoteDosisPorIndice(indice);

            if (correcto) {
                System.out.println("Orden ejecutada correctamente.");
            } else {
                System.out.println("Índice no existente");
            }
        } catch (Exception e) {

            System.out.println("Formato incorrecto.");

        }

    }

    public void bajaEmpleadoManual() {

        Scanner sc = new Scanner(System.in);

        System.out.println("");
        System.out.println("Menu -> Baja Empleado");
        System.out.println("Teclee el identificador del empleado a dar de baja:");
        String identificador = sc.nextLine();

        while (identificador.equals("")) {
            System.out.println("Identificador incorrecto. Tecleelo de nuevo:");
            identificador = sc.nextLine();
        }

        System.out.println("¿Está seguro que quiere dar de baja a " + identificador + "? (y/n)");
        String confirmacion = sc.nextLine();

        while (!confirmacion.equals("y") && !confirmacion.equals("n")) {
            System.out.println("Introduzca y o n:");
            confirmacion = sc.nextLine();
        }

        if (confirmacion.equals("y")) {

            boolean correcto = centro.darEmpleadoDeBaja(identificador);

            if (correcto) {
                System.out.println("Orden ejecutada correctamente.");
            } else {
                System.out.println("Usuario no existente o sin permisos");
            }
        } else if (confirmacion.equals("n")) {
            System.out.println("Orden cancelada");
        }

    }

    public void bajaEmpleadoSeleccion() {

        System.out.println("");
        System.out.println("Menu -> Baja Empleado");
        System.out.println("Usuarios disponibles: ");
        centro.mostrarInfoEmpleadosConNumero(usuarioLogueado.isAdmin());

        Scanner sc = new Scanner(System.in);
        try {
            System.out.println("Teclee la opción del empleado a dar de baja:");
            int opcion = sc.nextInt();

            while (opcion < 0) {
                System.out.println("Opción incorrecto. Teclee la opción de nuevo:");
                opcion = sc.nextInt();
            }

            System.out.println("¿Está seguro que quiere dar de baja al mepleado número " + opcion + "? (y/n)");
            String confirmacion = sc.nextLine();

            while (!confirmacion.equals("y") && !confirmacion.equals("n")) {
                System.out.println("Introduzca y o n:");
                confirmacion = sc.nextLine();
            }

            if (confirmacion.equals("y")) {

                boolean correcto = centro.darEmpleadoDeBaja(opcion);

                if (correcto) {
                    System.out.println("Orden ejecutada correctamente.");
                } else {
                    System.out.println("Usuario no existente o sin permisos");
                }
            } else if (confirmacion.equals("n")) {
                System.out.println("Orden cancelada");
            }

        } catch (Exception e) {
            System.out.println("Opción incorrecta.");
        }

    }

    public void menuAñadirVacunas() throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("");
        System.out.println("Menu -> AñadirVacunas");
        System.out.println("Introduce el ID del nuevo lote (0 para cancelar)");
        int id = -1;
        do {
            try {
                String lectura = br.readLine();
                id = Integer.parseInt(lectura);
            } catch (NumberFormatException ex) {
                //Si salta la excepción es porque la lectura no es un numero entero
                id = -1;
            }

            if (id == 0) {
                System.out.println("Cancelado");
                return;
            }
            if (id < 0) {
                System.out.println("Id inválido, vuelve a teclearlo:");
            }

        } while (id < 0);

        System.out.println("Introduce el nombre del fabricante del lote:");
        String fabricante = br.readLine();

        while (fabricante.isEmpty()) {
            System.out.println("Nombre de fabricante inválido, vuelve a teclearlo:");
            fabricante = br.readLine();
        }

        System.out.println("Introduce las unidades del nuevo lote (0 para cancelar)");
        int unidades = -1;
        do {
            try {
                String lectura = br.readLine();
                unidades = Integer.parseInt(lectura);
            } catch (NumberFormatException ex) {
                //Si salta la excepción es porque la lectura no es un numero entero
                unidades = -1;
            }

            if (unidades == 0) {
                System.out.println("Cancelado");
                return;
            }
            if (unidades < 0) {
                System.out.println("Número de unidades inválido, vuelve a teclearlo:");
            }

        } while (unidades < 0);

        centro.añadirLoteVacunas(new LoteDosis(id, fabricante, unidades));

    }

    public void menuAñadirUsuario() throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("");
        System.out.println("Menu -> AñadirUsuario");
        System.out.println("Introduce el nss del nuevo usuario (0 para cancelar)");
        String nss = "";
        do {
            nss = br.readLine();
            if (nss.equals("0")) {
                System.out.println("Cancelado");
                return;
            }
            if (nss.equals("")) {
                System.out.println("NSS inválido, vuelve a teclearlo:");
            }

        } while (nss.equals(""));

        System.out.println("Introduce el nombre del nuevo usuario (0 para cancelar)");
        String nombre = "";
        do {

            nombre = br.readLine();

            if (nombre.equals("0")) {
                System.out.println("Cancelado");
                return;
            }
            if (nombre.equals("")) {
                System.out.println("Nombre inválido, vuelve a teclearlo:");
            }

        } while (nombre.equals(""));

        System.out.println("Introduce el DNI del nuevo usuario (0 para cancelar)");
        String dni = "";
        do {

            dni = br.readLine();

            if (dni.equals("0")) {
                System.out.println("Cancelado");
                return;
            }
            if (dni.equals("")) {
                System.out.println("DNI inválido, vuelve a teclearlo:");
            }

        } while (dni.equals(""));

        System.out.println("Introduce la dirección del nuevo usuario (0 para cancelar)");
        String direccion = "";
        do {

            direccion = br.readLine();

            if (direccion.equals("0")) {
                System.out.println("Cancelado");
                return;
            }
            if (direccion.equals("")) {
                System.out.println("Dirección inválida, vuelve a teclearlo:");
            }

        } while (direccion.equals(""));

        System.out.println("Introduce el teléfono del nuevo usuario (0 para cancelar)");
        String telefono = "";
        do {

            telefono = br.readLine();

            if (telefono.equals("0")) {
                System.out.println("Cancelado");
                return;
            }
            if (telefono.equals("")) {
                System.out.println("Teléfono inválido, vuelve a teclearlo:");
            }

        } while (telefono.equals(""));

        System.out.println("Introduce la edad del nuevo usuario (0 para cancelar)");
        int edad = -1;
        do {
            try {
                String lectura = br.readLine();
                edad = Integer.parseInt(lectura);
            } catch (NumberFormatException ex) {
                //Si salta la excepción es porque la lectura no es un numero entero
                edad = -1;
            }

            if (edad == 0) {
                System.out.println("Cancelado");
                return;
            }
            if (edad < 0) {
                System.out.println("Edad inválida, vuelve a teclearlo:");
            }

        } while (edad < 0);       

        centro.añadirUsuario(new Usuario(nss, nombre, dni, direccion, telefono, edad));

    }

    public void menuAñadirEmpleado() throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("");
        System.out.println("Menu -> AñadirEmpleado");
        System.out.println("Introduce el identificador del nuevo empleado (0 para cancelar)");
        String identificador = "";
        do {
            identificador = br.readLine();
            if (identificador.equals("0")) {
                System.out.println("Cancelado");
                return;
            }
            if (identificador.equals("")) {
                System.out.println("Identificador inválido, vuelve a teclearlo:");
            }

        } while (identificador.equals(""));

        System.out.println("Introduce el nombre del nuevo empleado (0 para cancelar)");
        String nombre = "";
        do {

            nombre = br.readLine();

            if (nombre.equals("0")) {
                System.out.println("Cancelado");
                return;
            }
            if (nombre.equals("")) {
                System.out.println("Nombre inválido, vuelve a teclearlo:");
            }

        } while (nombre.equals(""));

        System.out.println("Introduce la contraseña del nuevo empleado (0 para cancelar)");
        String contraseña = "";
        do {

            contraseña = br.readLine();

            if (contraseña.equals("0")) {
                System.out.println("Cancelado");
                return;
            }
            if (contraseña.equals("")) {
                System.out.println("Contraseña inválida, vuelve a teclearlo:");
            }

        } while (contraseña.equals(""));

        System.out.println("Introduce el teléfono del nuevo empleado (0 para cancelar)");
        String telefono = "";
        do {

            telefono = br.readLine();

            if (telefono.equals("0")) {
                System.out.println("Cancelado");
                return;
            }
            if (telefono.equals("")) {
                System.out.println("Teléfono inválido, vuelve a teclearlo:");
            }

        } while (telefono.equals(""));

        System.out.println("Introduce la fecha de incorporación del nuevo empleado (0 para cancelar)");
        String fechaIncorporacion = "";
        do {

            fechaIncorporacion = br.readLine();

            if (fechaIncorporacion.equals("0")) {
                System.out.println("Cancelado");
                return;
            }
            if (fechaIncorporacion.equals("")) {
                System.out.println("Fecha inválida, vuelve a teclearlo:");
            }

        } while (fechaIncorporacion.equals(""));

        System.out.println("Introduce la edad del nuevo empleado (0 para cancelar)");
        int edad = -1;
        do {
            try {
                String lectura = br.readLine();
                edad = Integer.parseInt(lectura);
            } catch (NumberFormatException ex) {
                //Si salta la excepción es porque la lectura no es un numero entero
                edad = -1;
            }

            if (edad == 0) {
                System.out.println("Cancelado");
                return;
            }
            if (edad < 0) {
                System.out.println("Edad inválida, vuelve a teclearlo:");
            }

        } while (edad < 0);     

        System.out.println("Introduce la dirección del nuevo usuario (0 para cancelar)");
        String direccion = "";
        do {

            direccion = br.readLine();

            if (direccion.equals("0")) {
                System.out.println("Cancelado");
                return;
            }
            if (direccion.equals("")) {
                System.out.println("Dirección inválida, vuelve a teclearlo:");
            }

        } while (direccion.equals(""));  

        centro.añadirEmpleado(new Empleado(identificador, nombre, contraseña, telefono, fechaIncorporacion, nombre, edad));

    }
    
    public void vacunarUsuario() throws IOException{
         BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("");
        System.out.println("Menu -> Vacunar Usuario");
        centro.mostrarInfoUsuariosConNumero();
        System.out.println("Introduce el índice del usuario a vacunar (-1 para cancelar)");
        int indiceUsuario = 0;
        int estado =0;
        do {
            try {
                String lectura = br.readLine();
                indiceUsuario = Integer.parseInt(lectura);
            } catch (NumberFormatException ex) {
                //Si salta la excepción es porque la lectura no es un numero entero
                indiceUsuario = -1;
            }

            if (indiceUsuario == -1) {
                System.out.println("Cancelado");
                return;
            }
            
            estado = centro.comprobarUsuario(indiceUsuario);
            if (estado == 0) {
                System.out.println("Índice inválido, vuelve a teclearlo:");
            }
            if (estado == 2) {
                System.out.println("Usuario ya vacunado, vuelve a teclearlo:");
            }

        } while (estado != 1);
        
        centro.mostrarInfoLoteVacunasConNumero();
        System.out.println("Introduce el índice del lote a usar (-1 para cancelar)");
        int indiceDosis = 0;
        estado = 0;
        do {
            try {
                String lectura = br.readLine();
                indiceDosis = Integer.parseInt(lectura);
            } catch (NumberFormatException ex) {
                //Si salta la excepción es porque la lectura no es un numero entero
                indiceDosis = -1;
            }

            if (indiceDosis == -1) {
                System.out.println("Cancelado");
                return;
            }
            
            estado = centro.comprobarLote(indiceDosis);
            if (estado == 0) {
                System.out.println("Índice inválido, vuelve a teclearlo:");
            }

        } while (estado != 1);
        
        System.out.println("Introduce el número de dosis a usar (-1 para cancelar)");
        int nDosis = 0;
        estado = 0;
        do {
            try {
                String lectura = br.readLine();
                nDosis = Integer.parseInt(lectura);
            } catch (NumberFormatException ex) {
                //Si salta la excepción es porque la lectura no es un numero entero
                nDosis = -1;
            }

            if (nDosis == -1) {
                System.out.println("Cancelado");
                return;
            }
            
            estado = centro.comprobarDosisEnElLote(indiceDosis, nDosis);
            if (estado == 0) {
                System.out.println("Dosis insuficientes, vuelve a teclearlo:");
            }

        } while (estado != 1);
        
        if(centro.vacunarUsuario(indiceDosis, indiceDosis, nDosis)){
            System.out.println("Vacunación correcta.");        
        }else{
            System.out.println("Vacunación incorrecta.");
        }
        
    }

    public void menuAdmin() throws IOException {

        System.out.println();

        System.out.println("Menu -> Admin");
        System.out.println("Has visto como no hacía nada");

    }

}
