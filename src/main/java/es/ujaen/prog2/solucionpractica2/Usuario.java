/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package es.ujaen.prog2.solucionpractica2;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Usuario {

    private String nss;
    private String nombre;
    private String dni;
    private String direccion;
    private String telefono;
    private int edad;
    private int nDosis;
    private String fabDosis;

    public Usuario(String nss, String nombre, String dni, String direccion, String telefono, int edad) {
        this.nss = nss;
        this.nombre = nombre;
        this.dni = dni;
        this.direccion = direccion;
        this.telefono = telefono;
        this.edad = edad;
        nDosis = 0;
        fabDosis = "";
    }

    public String getNss() {
        return nss;
    }

    public void setFabDosis(String fabDosis) {
        this.fabDosis = fabDosis;
    }

    public void setnDosis(int nDosis) {
        this.nDosis = nDosis;
    }

    public void registrarVacuna(LoteDosis lote, int unidades) {

        nDosis = unidades;
        fabDosis = lote.getFabricante();

    }

    public boolean yaVacunado() {
        if (nDosis > 0) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        String impresion = "Usuario | NSS: " + nss + " nombre: " + nombre
                + " dni: " + dni + " direccion: " + direccion + " telefono: " + telefono
                + " edad: " + edad + " nDosis: " + nDosis + " fabDosis: " + fabDosis;
        return impresion;
    }

}
