/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.solucionpractica2;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Empleado {
    
    private String identificador;
    private String nombre;
    private String contraseña;
    private String telefono;
    private String fechaIncorporacion;
    private String dni;
    private int edad;

    public Empleado(String identificador, String nombre, String contraseña, String telefono, String fechaIncorporacion, String dni, int edad) {
        this.identificador = identificador;
        this.nombre = nombre;
        this.contraseña = contraseña;
        this.telefono = telefono;
        this.fechaIncorporacion = fechaIncorporacion;
        this.dni = dni;
        this.edad = edad;
    }

    

    public boolean comprobarContraseña(String contraseñaIntroducida){
    
        return contraseña.equals(contraseñaIntroducida);
        
    }

    public String getIdentificador() {
        return identificador;
    }

    public String getFechaIncorporacion() {
        return fechaIncorporacion;
    }

    public String getDni() {
        return dni;
    }

    public int getEdad() {
        return edad;
    }

    

    public boolean isAdmin(){
    
        return identificador.equals("ADMIN");
        
    }
    
    @Override
    public String toString() {
        String impresion = "Empleado | ID: " + identificador + " nombre: " + nombre
                + " telefono: " + telefono;
        return impresion;
    }
    
}
