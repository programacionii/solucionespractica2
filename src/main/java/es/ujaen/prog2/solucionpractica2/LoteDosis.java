/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.solucionpractica2;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class LoteDosis {

    private int identificador;
    private String fabricante;
    private int dosisDisponibles;

    public LoteDosis(int identificador, String fabricante, int dosisDisponibles) {
        this.identificador = identificador;
        this.fabricante = fabricante;
        this.dosisDisponibles = dosisDisponibles;
    }

    public boolean retirarDosis(int unidades) {

        if (unidades > dosisDisponibles) {
            return false;
        } else {
            dosisDisponibles = dosisDisponibles - unidades;
            return true;
        }

    }

    public int getDosisDisponibles() {
        return dosisDisponibles;
    }

    public int getIdentificador() {
        return identificador;
    }

    public String getFabricante() {
        return fabricante;
    }

    @Override
    public String toString() {
        String impresion = "Lote Dosis | ID: " + identificador
                + " fabricante: " + fabricante + " dosis restantes: " + dosisDisponibles;
        return impresion;
    }

}
